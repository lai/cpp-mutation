#ifndef FIB_H
#define FIB_H

#ifdef __cplusplus
extern "C" {
#endif

int fib1(int n);
int fib2(int n);

#ifdef __cplusplus
}
#endif

#endif
