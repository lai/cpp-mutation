#include "catch.hpp"
#include "../fib.h"
#include "mutation.h"


TEST_CASE("fib")
{

    MUTATION_START(fib1, "fib.so")
    {
        typedef int (*fibfunc)(int);
        fibfunc f = (fibfunc) test_function();

        REQUIRE(f(6) == 8);
        REQUIRE(f(5) == 5);
        REQUIRE(f(4) == 3);
        REQUIRE(f(3) == 2);
        REQUIRE(f(2) == 1);
        REQUIRE(f(1) == 1);
    }
    MUTATION_END;


    MUTATION_START(fib2, "fib.so")
    {
        typedef int (*fibfunc)(int);
        fibfunc f = (fibfunc) test_function();

        REQUIRE(f(1) == 1);
        REQUIRE(f(2) == 1);
        REQUIRE(f(3) == 2);
        REQUIRE(f(4) == 3);
        REQUIRE(f(5) == 5);
        REQUIRE(f(6) == 8);

       
        REQUIRE(f(6) == 8);
        REQUIRE(f(5) == 5);
        REQUIRE(f(4) == 3);
        REQUIRE(f(3) == 2);
        REQUIRE(f(2) == 1);
        REQUIRE(f(1) == 1);
 

    }
    MUTATION_END;


}
